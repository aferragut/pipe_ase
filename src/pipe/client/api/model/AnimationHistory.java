package pipe.client.api.model;

import java.beans.PropertyChangeSupport;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * changes by Oswaldo Artiles 03.14.2018
 *
 */
import pipe.gui.FileSystemPipe;
import pipe.gui.GuiFrame;

/**
 * end changes by Oswaldo Artiles 03.14.2018
 *
 */

/**
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author$
 * @version $Revision$ $Date$
 */
public class AnimationHistory extends PropertyChangeSupport {

  private List<AnimationHistoryItem> mItems = new ArrayList<>(5);
  private AnimationHistoryItem mCurrentItem;
  private String mTitle;

  public AnimationHistory(final String pTitle) {
    this("", pTitle);
  }

  /**
   * Constructs a <code>PropertyChangeSupport</code> object.
   *
   * @param sourceBean The bean to be given as the source for any events.
   */
  public AnimationHistory(final Object sourceBean, final String pTitle) {
    super(sourceBean);
    mTitle = pTitle;
  }


  public AnimationHistoryItem addNewItem(final AnimationType pAnimationType, final String pTerminationCriteria) {
    AnimationHistoryItem item = new AnimationHistoryItem(this, "Run" + (mItems.size() + 1), pAnimationType, pTerminationCriteria);
    mItems.add(item);
    mCurrentItem = item;
    firePropertyChange("run.newItem", null, mCurrentItem);
    /**
     * changes by Oswaldo Artiles 03.14.2018
     *
     */
    String run = item.getTitle() + "\n";
    String type = item.getAnimationType() + "\n";
    String termination = item.getTerminationCriteria() +  "\n";
    PrintWriter out = GuiFrame.getOutPrintWriter();
    FileSystemPipe.writeRun(out, run, type, termination);

    System.out.println( item.getTitle());
    System.out.println( item.getAnimationType());
    System.out.println( item.getTerminationCriteria());

    /**
     * end changes by Oswaldo Artiles 03.14.2018
     *
     */
    return item;
  }

  public String getTitle() {
    return mTitle;
  }


  public AnimationHistoryItem getCurrentItem() {
    return mCurrentItem;
  }
}
