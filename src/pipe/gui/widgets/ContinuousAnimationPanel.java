package pipe.gui.widgets;

import formula.GlobalClock;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.internal.chartpart.Chart;
import org.knowm.xchart.style.Styler;
import pipe.client.api.model.AnimationHistoryItem;
import pipe.client.api.model.AnimationType;
import pipe.dataLayer.DataLayer;
import pipe.dataLayer.Place;
import pipe.dataLayer.Transition;
import pipe.gui.Animator;
import pipe.gui.CreateGui;
/**
 * changes by Oswaldo Artiles 03.13.2018
 */
import formula.parser.Symbol;
import formula.parser.SymbolTable;
import pipe.dataLayer.Token;
import pipe.gui.FileSystemPipe;
import pipe.gui.GuiFrame;
import java.io.PrintWriter;
/**
 * end  changes Oswaldo Artiles
 */

import javax.swing.*;
import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

/**
 *
 * Continuous animation panel
 *
 * @author <a href="mailto:dalam004@fiu.edu">Dewan Moksedul Alam</a>
 * @author last modified by $Author: $
 * @version $Revision: $ $Date: $
 */
public class ContinuousAnimationPanel extends JPanel {

    private final DataLayer mDataLayer;
    private final Animator mAnimator;
    List<Function<Void, Void>> mCharUpdaters = new ArrayList<>();
    private boolean mContinuousRunning = false;
    /**
     * changes by Oswaldo Artiles 03.13.2018
     */
    long startTime = 0;
    long endTime = 0;

    /**
     * end changes by Oswaldo Artiles
     */


    public ContinuousAnimationPanel(final Animator pAnimator, final DataLayer pDataLayer) {
        mDataLayer = pDataLayer;
        mAnimator = pAnimator;

        initComponents();
    }

    private void initComponents() {
        JPanel animationConfigPanel = initializeAnimationConfigPanel();
        JPanel chartConfigPanel = initiaizeChartConfigPanel();
        if (animationConfigPanel == null || chartConfigPanel == null) {
//            do nothing
        }

        JPanel configWrapper = new JPanel();
        configWrapper.setLayout(new BoxLayout(configWrapper, BoxLayout.X_AXIS));
        configWrapper.add(animationConfigPanel);
        configWrapper.add(chartConfigPanel);

        setPreferredSize(new Dimension(630, 300));
        add(configWrapper);
    }

    private JPanel initiaizeChartConfigPanel() {
        JPanel chartConfigPanel = new JPanel();
        chartConfigPanel.setLayout(new BoxLayout(chartConfigPanel, BoxLayout.Y_AXIS));

        JPanel xConfigPanel = new JPanel();
        xConfigPanel.setLayout(new BoxLayout(xConfigPanel, BoxLayout.Y_AXIS));

        JPanel xPlaceWrapperPanel = new JPanel();
        xPlaceWrapperPanel.setLayout(new BoxLayout(xPlaceWrapperPanel, BoxLayout.X_AXIS));
        xPlaceWrapperPanel.add(new JLabel("x-Axis place:"));

        final JComboBox<Place> xPlaceCombobox = new JComboBox<>(mDataLayer.getPlaces());
        xPlaceWrapperPanel.add(xPlaceCombobox);

        JPanel xValueIndexWrapperPanel = new JPanel();
        xValueIndexWrapperPanel.setLayout(new BoxLayout(xValueIndexWrapperPanel, BoxLayout.X_AXIS));
        xValueIndexWrapperPanel.add(new JLabel("X-Axis Value Index: "));
        JTextField xValueIndexTextField = new JTextField("0");
        xValueIndexWrapperPanel.setToolTipText("Index (0 based) of the field in the datatype that you want to show in the chart");
        xValueIndexWrapperPanel.add(xValueIndexTextField);
        xConfigPanel.add(xPlaceWrapperPanel);

        JCheckBox xAxisCheckBox = new JCheckBox("Use Time as X-Axis reference instead");
        xConfigPanel.add(xAxisCheckBox);

        /**
         * changes by Alejandro 1
         */
        JCheckBox showAllSeries = new JCheckBox("Display All Series vs Time");
        xConfigPanel.add(showAllSeries);

        /**
         * end changes by Alejandro 1
         */

        xConfigPanel.add(xValueIndexWrapperPanel);


        JPanel yConfigPanel = new JPanel();
        yConfigPanel.setLayout(new BoxLayout(yConfigPanel, BoxLayout.Y_AXIS));

        JPanel yPlaceWrapperPanel = new JPanel();
        yPlaceWrapperPanel.setLayout(new BoxLayout(yPlaceWrapperPanel, BoxLayout.X_AXIS));
        yPlaceWrapperPanel.add(new JLabel("Y-Axis place:"));
        final JComboBox<Place> yPlaceCombobox = new JComboBox<>(mDataLayer.getPlaces());
        yPlaceWrapperPanel.add(yPlaceCombobox);

        JPanel yValueIndexWrapperPanel = new JPanel();
        yValueIndexWrapperPanel.setLayout(new BoxLayout(yValueIndexWrapperPanel, BoxLayout.X_AXIS));
        yValueIndexWrapperPanel.add(new JLabel("Y-Axis Value index: "));
        yValueIndexWrapperPanel.setToolTipText("Index (0 based) of the field in the datatype that you want to show in the chart");
        JTextField yvalueIndexTextField = new JTextField();
        yValueIndexWrapperPanel.add(yvalueIndexTextField);
        yConfigPanel.add(yPlaceWrapperPanel);
        yConfigPanel.add(yValueIndexWrapperPanel);

        JButton addChartButton = new JButton("Add chart");
        addChartButton.addActionListener(l -> {
            int yIndex = Integer.parseInt(yvalueIndexTextField.getText().trim());

            /**
             * changes by Alejandro 2
             */
            if(showAllSeries.isSelected()){

                Place[] places = mDataLayer.getPlaces();
                addTimedChartAllSeries("Time",  places, yIndex);

            }else{

                if (xAxisCheckBox.isSelected()) {

                    addTimedChart("Time", (Place) yPlaceCombobox.getSelectedItem(), yIndex);
                }
                else {
                    int xIndex = Integer.parseInt(xValueIndexTextField.getText().trim());
                    addChart((Place)xPlaceCombobox.getSelectedItem(), xIndex, (Place) yPlaceCombobox.getSelectedItem(), yIndex);
                }
            }

        });
        /**
         * end changes by Alejandro 2
         */

        chartConfigPanel.add(xConfigPanel);
        chartConfigPanel.add(yConfigPanel);
        chartConfigPanel.add(addChartButton);

        chartConfigPanel.setBorder(BorderFactory.createTitledBorder("Chart config"));
        return chartConfigPanel;
    }

    /**
     * changes by Alejandro 3
     */
    protected void addTimedChartAllSeries(final String pXOption, Place[] places, final int pYIndex) {
        String yTitle = "Series";//pYPlace.getName();
        String chartTitle = String.format("%s-%s XY Chart", pXOption, yTitle);

        final List<List<Double>> xData2D = new ArrayList<>();//TODO: 2D array: so it has the lines
        final List<List<Double>> yData2D = new ArrayList<>();

        addChart(pXOption, yTitle, chartTitle, new ChartUpdater() {
            @Override
            public ArrayList<String> addSeries(XYChart pChart) {

                ArrayList<String> series = new ArrayList<String>();//= "series1";
                String name = "series";

                for (int i = 0; i < places.length; i++) {

                    List<Double> xData = new ArrayList<>();
                    List<Double> yData = new ArrayList<>();


                    xData.add((double) GlobalClock.getInstance().getClockState().next);
                    yData.add( places[i].getToken().getListToken().get(0).Tlist.get(pYIndex).getValueAsDouble());
                    series.add(name+(i+1));
                    pChart.addSeries(name+(i+1), xData, yData);

                    xData2D.add(xData);
                    yData2D.add(yData);
                }

                return series;
            }//end addSeries

            @Override
            public void updateSeries(XYChart pChart, ArrayList<String> pSeries) {

                //ON UPDATE, Y matches X size:
                for (int i = 0; i < xData2D.size(); i++) {

                    xData2D.get(i).add((double) GlobalClock.getInstance().getClockState().next);
                    yData2D.get(i).add( places[i].getToken().getListToken().get(0).Tlist.get(pYIndex).getValueAsDouble());

                    pChart.updateXYSeries(pSeries.get(i), xData2D.get(i), yData2D.get(i), null);

                }

            }
        });
    }
    /**
     * end changes by Alejandro 3
     */

    private void addTimedChart(final String pXOption, final Place pYPlace, final int pYIndex) {
        String yTitle = pYPlace.getName();
        String chartTitle = String.format("%s-%s XY Chart", pXOption, yTitle);

        final List<Double> xData = new ArrayList<>();
        final List<Double> yData = new ArrayList<>();

        xData.add((double) GlobalClock.getInstance().getClockState().next);
        yData.add(pYPlace.getToken().getListToken().get(0).Tlist.get(pYIndex).getValueAsDouble());

        addChart(pXOption, yTitle, chartTitle, new ChartUpdater() {
            @Override
            /**
             * changes by Alejandro 4
             */
            public ArrayList<String> addSeries(XYChart pChart) {
                String series = "series1";
                pChart.addSeries(series, xData, yData);
                return new ArrayList<String>(Arrays.asList(series));
            }

            @Override
            public void updateSeries(XYChart pChart, ArrayList<String> pSeries) {
                xData.add((double) GlobalClock.getInstance().getClockState().next);
                yData.add(pYPlace.getToken().getListToken().get(0).Tlist.get(pYIndex).getValueAsDouble());
                pChart.updateXYSeries(pSeries.get(0), xData, yData, null);
            }
            /**
             * end changes by Alejandro 4
             */
        });
    }

    private void displayChart(final String pTitle, final JPanel pChartPanel) {
        final JFrame frame = new JFrame(pTitle);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frame.add(pChartPanel);
                frame.pack();
                frame.setVisible(true);
            }
        });
    }



    private void addChart(final Place xPlace, final int xIndex, final Place yPlace, final int yIndex) {
        String xTitle = xPlace.getName();
        String yTitle = yPlace.getName();
        String chartTitle = String.format("%s-%s XY Chart", xTitle, yTitle);

        final List<Double> xData = new ArrayList<>();
        final List<Double> yData = new ArrayList<>();

        xData.add(xPlace.getToken().getListToken().get(0).Tlist.get(xIndex).getValueAsDouble());
        yData.add(yPlace.getToken().getListToken().get(0).Tlist.get(yIndex).getValueAsDouble());

        addChart(xTitle, yTitle, chartTitle, new ChartUpdater() {
            @Override
            /**
             * changes by Alejandro 5
             */
            public ArrayList<String> addSeries(XYChart pChart) {
                String series = "series1";
                pChart.addSeries(series, xData, yData);
                return new ArrayList<String>(Arrays.asList(series));
            }

            @Override
            public void updateSeries(XYChart pChart,  ArrayList<String> pSeries) {
                xData.add(xPlace.getToken().getListToken().get(0).Tlist.get(xIndex).getValueAsDouble());
                yData.add(yPlace.getToken().getListToken().get(0).Tlist.get(yIndex).getValueAsDouble());
                pChart.updateXYSeries(pSeries.get(0), xData, yData, null);
            }
            /**
             * end changes by Alejandro 5
             */
        });
    }

    private void addChart(final String pTitleX, final String pTitleY, final String pChartTitle, final ChartUpdater pChartUpdater) {
        XYChart xyChart = new XYChartBuilder().width(500)
                .height(400)
                .xAxisTitle(pTitleX)
                .yAxisTitle(pTitleY)
                .theme(Styler.ChartTheme.Matlab)
                .title(pChartTitle).build();

        /**
         * changes by Alejandro 6
         */
        final ArrayList<String> series = pChartUpdater.addSeries(xyChart);
        /**
         * end changes by Alejandro 6
         */
        final XChartPanel<XYChart> chartPanel = new XChartPanel(xyChart);
        mCharUpdaters.add(new Function<Void, Void>() {
            @Override
            public Void apply(Void aVoid) {
                pChartUpdater.updateSeries(xyChart, series);
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        chartPanel.revalidate();
                        chartPanel.repaint();
                    }
                });
                return null;
            }
        });

        displayChart(xyChart.getTitle(), chartPanel);
    }


    private JPanel initializeAnimationConfigPanel() {
        JPanel buttonPannel = new JPanel();
        buttonPannel.setLayout(new BoxLayout(buttonPannel, BoxLayout.Y_AXIS));

        JPanel delayConfigPanel = new JPanel();
        delayConfigPanel.add(new JLabel("Delay in Seconds: "));
        final JTextField delayConfigTextField = new JTextField("0", 10);
        delayConfigTextField.setEditable(true);
        delayConfigPanel.add(delayConfigTextField);

        JPanel stepsConfigPanel = new JPanel();
        stepsConfigPanel.add(new JLabel("Maximum number of steps: "));
        final JTextField maxStepsConfigField = new JTextField("100", 10);
        maxStepsConfigField.setEditable(true);
        stepsConfigPanel.add(maxStepsConfigField);

        JPanel buttonHolder = new JPanel();
        JButton singleStep = new JButton("SingleStep");
        JButton continuous = new JButton("Continuous");
        JButton stop = new JButton("Stop");

        singleStep.addActionListener(l -> {
            /**
             * changes by Oswaldo Artiles 03.13.2018
             */

            int maxSteps = 1;
            buttonHolder.setEnabled(false);
            doSingleStepAnimation(maxSteps);
            buttonHolder.setEnabled(true);

            /**
             * end changes Oswaldo Artiles
             */

        });
        continuous.addActionListener(l -> {
            int delay = Integer.valueOf(delayConfigTextField.getText());
            int maxSteps = Integer.valueOf(maxStepsConfigField.getText());
            buttonHolder.setEnabled(false);
            doContinuousAnimation(maxSteps, delay);
            buttonHolder.setEnabled(true);
//            JOptionPane.showMessageDialog(ContinuousAnimationPanel.this, "Not implemented yet.");
        });
        stop.addActionListener(l -> {
            mContinuousRunning = false;
//            JOptionPane.showMessageDialog(ContinuousAnimationPanel.this, "Not implemented yet.");
        });

        buttonHolder.add(singleStep);
        buttonHolder.add(continuous);
        buttonHolder.add(stop);

        buttonPannel.add(delayConfigPanel);
        buttonPannel.add(stepsConfigPanel);
        buttonPannel.add(buttonHolder);

        buttonPannel.setBorder(BorderFactory.createTitledBorder("Animation config"));

        return buttonPannel;
    }

    private void doContinuousAnimation(final int pMaxSteps, final int pDelay) {
        int count = 0;
        if (mContinuousRunning == false) {
            mContinuousRunning = true;
            while (count < pMaxSteps && mContinuousRunning) {
                /**
                 * changes by Oswaldo Artiles 03.13.2018
                 */
                doSingleStepAnimation(pMaxSteps);
                /**
                 * end changes by Oswaldo Artiles
                 */

                count++;
                if (pDelay > 0) {
                    try {
                        Thread.sleep(TimeUnit.SECONDS.toMillis(pDelay));
                    } catch (InterruptedException e) {
                        mContinuousRunning = false;
                        throw new RuntimeException(e);
                    }
                }
            }
        }

    }

    /**
     * changes by Oswaldo Artiles 03.13.2018
     */
    private void doSingleStepAnimation(final int pMaxSteps) {
        startTime = System.nanoTime();
        String terminationCriteria = String.format("Number of steps %d", pMaxSteps);
        AnimationHistoryItem animationHistoryItem = CreateGui.getAnimationHistory(CreateGui.getTab().getSelectedIndex()).addNewItem(AnimationType.FixedStepAnimation,
                terminationCriteria);

        List<String> pInputSymbols = new ArrayList<>(2);
        List<String> pOutputSymbols = new ArrayList<>(2);

        /**
         * end changes by Oswaldo Artiles
         */
        Transition fired = mAnimator.doHighLevelRandomFiring();
        boolean isContinuousTransitionFired = false;
        if (fired != null) {
            /**
             * changes by Oswaldo Artiles 03.13.2018
             */
            //System.out.println("ContinuousAnimationPanel.doSingleStepAnimation:fired not null");
            /**
             * end changes by Oswaldo Artiles
             */
            addFiredTransitionHistory(animationHistoryItem, fired);
            isContinuousTransitionFired = fired.isContinuous();
        }
        for (Transition transition : mDataLayer.continuous) {
            if (transition != fired) {
                if (transition.checkStatusAndFireWhenEnabled()) {
                    /**
                     * changes by Oswaldo Artiles 03.13.2018
                     */
                    //System.out.println("ContinuousAnimationPanel.doSingleStepAnimation:transition enabled");
                    long transitionStartTime = System.nanoTime();
                    mAnimator.fireHighLevelTransitionInGUI(transition);
                    long transitionEndTime = System.nanoTime();
                    SymbolTable symTable = transition.getTransSymbolTable();
                    pInputSymbols.clear();
                    pOutputSymbols.clear();
                    System.out.println("ContinuousAnimationPanel.doSingleStepAnimation:symbol table");
                    symTable.printSymTable();
                    int i = 0;
                    for(Symbol sym : symTable.getValues()) {
                        if (sym.getType() != Symbol.TYPE.MULTI) {
                            //System.out.println(sym.getKey() + " animator  " + ((Token) sym.getBinder()).displayToken());
                            if ( i == 0) {
                                pInputSymbols.add(sym.getKey());
                                pInputSymbols.add(((Token) sym.getBinder()).displayToken());
                                System.out.println("Animator.Input token combinations: " + pInputSymbols.get(0)+"   " +pInputSymbols.get(1));
                                i = i +1;
                            }else {
                                pOutputSymbols.add(sym.getKey());
                                pOutputSymbols.add(((Token) sym.getBinder()).displayToken());
                                System.out.println("Animator.Output token combinations: " + pOutputSymbols.get(0)+"   " +pOutputSymbols.get(1));
                            }
                        }
                    }
                    //System.out.println("ContinuousAnimationPanel.doSingleStepAnimation:clean table " );
                    symTable.cleanTable();

                    System.out.println("ContinuousAnimationPanel.doSingleStepAnimation:transition time "+ (transitionEndTime - transitionStartTime) );
                    AnimationHistoryItem.TransitionItem item = new AnimationHistoryItem.TransitionItem(transition.getName(),
                            (transitionEndTime - transitionStartTime), pInputSymbols, pOutputSymbols);

                    //System.out.println("ContinuousAnimationPanel.doSingleStepAnimation:transition time 2 "+ (transitionEndTime - transitionStartTime) );
                    animationHistoryItem.addFiredTransition(item);
                    //System.out.println("ContinuousAnimationPanel.doSingleStepAnimation:transition time 3 ");

                    endTime = System.nanoTime();
                    System.out.println("ContinuousAnimationPanel.doSingleStepAnimation:time " + (endTime - startTime) );
                    animationHistoryItem.setTimeTakenInNanos(endTime - startTime);
                    /**
                     * changes by Oswaldo Artiles 03.14.2018
                     * writing results of simulation to output file
                     */
                    String time = "time: " + animationHistoryItem.getTimeTakenInMillis() +  " milliSeconds" + "\n";
                    PrintWriter out = GuiFrame.getOutPrintWriter();
                    FileSystemPipe.writeRunTime(out,time);
                    String firedTransName = "Transition fired: " +transition.getName()+ "\n";
                    String outTokens = "Output tokens: " + pOutputSymbols.get(0)+"   " +pOutputSymbols.get(1) + "\n";
                    String inTokens = "Output tokens: " +  pInputSymbols.get(0)+"   " +pInputSymbols.get(1) + "\n";
                    String timeTrans = "time: " + (transitionEndTime - transitionStartTime)+ " nanoSeconds" + "\n";
                    FileSystemPipe.writeTransition(out, firedTransName,inTokens,outTokens,timeTrans);

                    /**
                     * end changes by Oswaldo Artiles 03.14.2018
                     *
                     */

                //addFiredTransitionHistory(animationHistoryItem, transition);

                    isContinuousTransitionFired = true;
                    //System.out.println("ContinuousAnimationPanel.doSingleStepAnimation: fired true ");
                    Iterator<Transition> iDepTrans = transition.getDependentTrans().iterator();
                    while (iDepTrans.hasNext()) {
                        Transition thisDepTrans = iDepTrans.next();
                        if (!thisDepTrans.isContinuous() && mDataLayer.disabled.remove(thisDepTrans)) {
                            mDataLayer.unknown.add(thisDepTrans);
                        }
                    }
                }
            }
        }

        if (isContinuousTransitionFired) {
            mCharUpdaters.forEach(f -> f.apply(null));
        }

        try(FileOutputStream fos = new FileOutputStream("continuous.txt", true)) {
            for (Place place : CreateGui.currentPNMLData().getPlaces()) {
                String token = "0";
                if (!place.token.getListToken().isEmpty()) {
                    token = place.token.getListToken().get(0).displayToken(false);
                }
                fos.write(String.format("%12s", token).getBytes());
            }
            fos.write(String.format("%n").getBytes());
            fos.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        GlobalClock.getInstance().increment();

    }

    private void addFiredTransitionHistory(final AnimationHistoryItem pHistoryItem, final Transition pTransition) {
        AnimationHistoryItem.TransitionItem item = new AnimationHistoryItem.TransitionItem(pTransition.getName(), 1000);
        pHistoryItem.addFiredTransition(item);
    }


    private interface ChartUpdater {
        //multiple series:
        /**
         * changes by Alejandro 7
         */
        ArrayList<String> addSeries(final XYChart pChart);
        void updateSeries(final XYChart pChart, ArrayList<String> series);
        /**
         * changes by Alejandro 7
         */
    }
}
