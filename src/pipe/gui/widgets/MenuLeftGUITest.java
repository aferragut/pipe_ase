package pipe.gui.widgets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.junit.Test;

import junit.framework.Assert;
import pipe.client.api.model.AnimationHistory;
import pipe.client.api.model.AnimationHistoryItem;
import pipe.client.api.model.AnimationType;
import pipe.client.ui.status.AnimationHistoryUI;
import pipe.dataLayer.Transition;
import pipe.gui.Animator;

/**
 * These are JUnits for:
 * 
 * the information displayed for the Transitions in the cascade menu of the left
 * panel of the GUI, both for the discrete and continuous systems.
 * 
 * the information displayed for the the Run in the cascade menu of the left
 * panel of the GUI for the simulation of continuous systems.
 * 
 * This JUnit could be run in a cron job and if it fails it will alert that
 * something was changed.
 * 
 * @author Alejandro Ferragut
 *
 */
public class MenuLeftGUITest {

	@Test
	public void test1() {
		//
		// AnimationHistory animH = new AnimationHistory("test");
		// AnimationHistoryUI animUI = new AnimationHistoryUI(animH);

		try {
			Animator anim = new Animator();
			anim.fireHighLevelTransitionInGUI(new Transition(0, 0));
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.fail();

		}
	}

	@Test
	public void test2() {
		try {
			Animator anim = new Animator();
			anim.stepForward();
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.fail();

		}
	}

	@Test
	public void test3() {

		try {
			Animator anim = new Animator();
			anim.stepBack();
			Assert.fail();
		} catch (Exception e) {
			Assert.assertTrue(true);

		}
	}

}