package pipe.gui.widgets;

import java.io.File;
import java.io.PrintWriter;

import org.junit.Test;

import junit.framework.Assert;
import pipe.gui.FileSystemPipe;

/**
 * 
 * @author Alejandro Ferragut
 *
 */
public class FileSystemPipeTest {

	@Test
	public void test1() {
		try {
			String name = "tutorialExample";
			File output = FileSystemPipe.createNewFile(name);
			String run = "Run" + 1 + "\n";
			String type = "Fixed Steps Animation" + "\n";
			String termination = "Number of Steps 6" + "\n";
			String time = "time: " + 80 + " milliSeconds" + "\n";
			String transition = "Transition fired: " + "Pay" + "\n";
			String outputTokens = "Output tokens: " + "y   <u3,-4.598>" + "\n";
			String inputTokens = "Output tokens: " + "x   <u3,-5.598>" + "\n";
			String timeTrans = "time: " + 700000 + " nanoSeconds" + "\n";
			PrintWriter out = new PrintWriter(output);

			FileSystemPipe.writeRun(out, run, type, termination);
			FileSystemPipe.writeRunTime(out, time);
			FileSystemPipe.writeTransition(out, transition, outputTokens, inputTokens, timeTrans);

			out.close();

			Assert.assertTrue(true);

		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	
	@Test
	public void createFileTest(){
		File output = FileSystemPipe.createNewFile("test");
		
		Assert.assertNotNull(output);
	}
	
	
	@Test
	public void testInvalidData() {
		//user clicks save with invalid data
		try {
			String name = "tutorialExample";
			File output = FileSystemPipe.createNewFile(name);
			String run =null;
			String type = null;
			String termination = null;
			String time = null;
			String transition = null;
			String outputTokens = null;
			String inputTokens = null;
			String timeTrans = null;
			PrintWriter out = new PrintWriter(output);

			FileSystemPipe.writeRun(out, run, type, termination);
			FileSystemPipe.writeRunTime(out, time);
			FileSystemPipe.writeTransition(out, transition, outputTokens, inputTokens, timeTrans);

			out.close();

			Assert.assertTrue(true);

		} catch (Exception e) {
			Assert.fail();
		}
	}
	

}
