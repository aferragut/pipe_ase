package pipe.gui.widgets;

import org.junit.Test;

import junit.framework.Assert;
import pipe.dataLayer.BasicType;
import pipe.dataLayer.DataLayer;
import pipe.dataLayer.Place;
import pipe.dataLayer.Token;
import pipe.dataLayer.abToken;
import pipe.gui.Animator;

/**
 * JUnit test for addTimedChartAllSeries method which displays multiple series
 * in a UI chart. Uses white box testing.
 * 
 * This JUnit could be run in a cron job and if it fails it will alert that
 * something was changed.
 * 
 * @author Alejandro Ferragut
 *
 */
public class MultipleSeriesUITest {

	@Test
	public void multipleSeries() {

		Place series1 = getPlaceforTesting("1.1");
		Place series2 = getPlaceforTesting("2.2");

		Place[] places = { series1, series2 };

		ContinuousAnimationPanel aPanel = new ContinuousAnimationPanel(new Animator(), new DataLayer());

		try {
			aPanel.addTimedChartAllSeries("time", places, 1);

		} catch (Exception e) {
			Assert.fail();
		}

	}

	@Test
	public void threeMultipleSeries() {

		Place series1 = getPlaceforTesting("1.1");
		Place series2 = getPlaceforTesting("2.2");
		Place series3 = getPlaceforTesting("3.3");

		Place[] places = { series1, series2, series3 };

		ContinuousAnimationPanel aPanel = new ContinuousAnimationPanel(new Animator(), new DataLayer());

		try {
			aPanel.addTimedChartAllSeries("time", places, 1);

		} catch (Exception e) {
			Assert.fail();
		}

	}

	@Test
	public void singeSerie() {
		// case where the user clicks multiple series but they only have one.
		Place series1 = getPlaceforTesting("1.1");

		Place[] places = { series1 };

		ContinuousAnimationPanel aPanel = new ContinuousAnimationPanel(new Animator(), new DataLayer());

		try {
			aPanel.addTimedChartAllSeries("time", places, 1);

		} catch (Exception e) {
			Assert.fail();
		}

	}

	/**
	 * helper method to initiate a place object
	 * 
	 * @param value
	 * @return a place with a value
	 * @author Alejandro Ferragut
	 */
	public Place getPlaceforTesting(String value) {

		// initialization for series
		Place series1 = new Place(0, 0);
		abToken one = new abToken();
		Token oneToken = new Token();
		BasicType basicType = new BasicType(0, value);
		BasicType[] basicTypeArray = new BasicType[2];
		basicTypeArray[1] = basicType;
		oneToken.add(basicTypeArray);
		one.addToken(oneToken);
		series1.setToken(one);
		return series1;
	}

}
