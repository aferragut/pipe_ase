/**
 * class written by Oswaldo Artiles, March 14 2018.
 *
 * ************************************************************
 *
 * This class:
 *
 * 1. Creates a new file and throws FileAlreadyExistsException.
 *
 * 2. Write in an output file the input and output tokens out of a
 * transition in PIPE+  after running  a  simulation. The
 * firing time of the transition is also written in the output file.
 *
 * 3. Write in an output file type of simulation, the criteria to
 * terminate the simulation and the total time of the  simulation in
 * PIPE+  after running the  simulation.
 */

package pipe.gui;

import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;




public class FileSystemPipe {

    /**
     * main program of the FileSystemPipe  class
     * @param args
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        String name = "tutorialExample";
        File output = createNewFile(name);
        String run = "Run"+ 1 + "\n";
        String type = "Fixed Steps Animation" + "\n";
        String termination = "Number of Steps 6" +  "\n";
        String time = "time: " + 80 +  " milliSeconds" + "\n";
        String transition = "Transition fired: " +"Pay" + "\n";
        String outputTokens = "Output tokens: " + "y   <u3,-4.598>" + "\n";
        String inputTokens = "Output tokens: " +  "x   <u3,-5.598>" + "\n";
        String timeTrans = "time: " + 700000+ " nanoSeconds" + "\n";
        PrintWriter out = new PrintWriter(output);

        writeRun(out, run, type, termination);
        writeRunTime(out, time);
        writeTransition(out,transition,outputTokens,inputTokens, timeTrans);

        out.close();

    }

    /**
     * Creates a new file and throws FileAlreadyExistsException.
     * @param name name of the file
     * @return
     */

    public static File createNewFile(String name) {
        String path = name + ".txt";
        File file = new File(path);
        System.out.println("\n FileSystemPipe: The path of the output file: " + file.getName() +"  is:");
        System.out.println(file.getAbsolutePath() + "\n");


        try {
            // Create the empty file with default permissions, etc.
            file.createNewFile();
        } catch (FileAlreadyExistsException x) {
            System.err.format("file named %s" +
                    " already exists%n", file);
        } catch (IOException x) {
            // Some other sort of failure, such as permissions.
            System.err.format("createFile error: %s%n", x);
        }
        return file;
    } // end of main


    /**
     * Write in an output file the input tokens in  and output
     * tokens out of a transition, and the firing transition time,
     * after running  a  simulation in PIPE+.
     *
     * @param out          PrintWriter to write in a file
     * @param transition   name of the transition
     * @param outTokens    tokens out of the transition
     * @param inTokens     tokens into the transition
     * @param time         firing time of the transition
     */
    public static void writeTransition(PrintWriter out, String transition, String inTokens, String outTokens,
                                       String time) {

        out.append(transition);
        out.append(inTokens);
        out.append(outTokens);
        out.append(time);
        out.append("\n");

    } //end of writeTransition method


    /**
     * Write in an output file: the type of a simulation and the criteria to terminate
     * a  simulation in PIPE+.
     *
     * @param out           PrintWriter to write in a file
     * @param run           number of the simulation
     * @param type          type of simulation
     * @param termination   the criteria to terminate the simulation.
     */
    public static void writeRun(PrintWriter out, String run, String type, String termination) {

        out.append(run);
        out.append(type);
        out.append(termination);

    } //end of writeRun method


    /**
     * Write in an output file:  the total time of a  simulation in PIPE+.
     *
     * @param out   PrintWriter to write in a file
     * @param time  total time of a  simulation
     */
    public static void writeRunTime(PrintWriter out, String time ) {

        out.append(time);
        out.append("\n");

    } //end of writeRunTime method


} //end of FileSystemPipe class



